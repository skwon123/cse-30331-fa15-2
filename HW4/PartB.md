Homework 4 - Part B
===================
Due 2015/10/29 at the beginning of class.

Q3. What are the advantages and disadvantages of separate chaining and open addressing?



Q4. A hash function should ideally have which properties? Circle all that apply.
a. It uniformly distributes all possible keys throughout the array.
b. It can be computed quickly.
c. Its range (return value) is an integer.
d. If two keys are equivalent, then they have the same hash value.
