Homework 3 - Part B
===================
Due 2015/10/01 at the beginning of class.

Q3: What are the main differences between a binary search tree and a B-tree?



Q4: Consider a B-tree in which the maximum number of entries in a node is 4. What is the minimum number of entries in any non-root node?
