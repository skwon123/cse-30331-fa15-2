Homework 3 - Part A
===================
Due 2015/09/29 at the beginning of class.

Q1: What are the stopping cases for the recursive binary search function?



Q2: The following numbers are inserted into an empty binary search tree in the given order: 10, 1, 3, 5, 15, 12, 16. What is the longest root-to-leaf path in the resulting tree?
