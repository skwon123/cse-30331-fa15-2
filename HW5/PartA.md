Homework 5 - Part A
===================
Due 2015/11/10 at the beginning of class.

Q1: Which statement or statements are true?
    A. A loop is the same thing as a cycle.
    B. In a directed graph, if there is an edge from u to v and an edge from v to u, then the edges are called multiple edges.
    C. In an undirected graph, the order of two connected vertices is unimportant. 
    D. Every problem can be represented as a graph.





Q2:    Below is a graph represented as an adjacency matrix. Does it have any loops? If so, where?

| 0 | 1 | 2 | 3
--|---|---|---|---
0 | T | F | F | T	
1 | F | F | F | T
2 | T | T | T | F	
3 | F | F | T | F