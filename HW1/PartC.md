
Homework 1 - Part C
===================

Due 2015/09/08 at the beginning of class.


Q1: Using Big-O notation, give the worst-case times for each of the following stack operations, for a stack of size N that is implemented using a linked list.


Empty:
Size:
Push:
Pop:
Peek: 

Q2: The STL queue is a container adapter that wraps around another underlying sequence container.  Given a choice among a vector, list, or deque, which sequence container you would choose as the underlying container for a queue, and why?
 
